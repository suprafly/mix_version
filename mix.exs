defmodule MixVersion.MixProject do
  use Mix.Project

  def project do
    [
      app: :mix_version,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: releases(),
      preferred_cli_env: [release: :prod]
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {MixVersion, []}
    ]
  end

  defp deps do
    [
      {:bakeware, git: "https://github.com/spawnfest/bakeware"}
    ]
  end

  defp releases do
    [
      mix_version: [
        steps: [:assemble, &Bakeware.assemble/1],
        overwrite: true,
        strip_beams: Mix.env() == :prod
      ]
    ]
  end
end
