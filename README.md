# Mix Version

A lightweight script and Mix Task for updating your project's version number from the commandline.

### Building and running with Bakeware

This project is using the new [Bakeware](https://github.com/bake-bake-bake/bakeware) library to build an executable that can be run across projects.

Change to this directory and run the following:

```sh
export MIX_ENV=prod
mix deps.get
mix release
```

This will generate an executable located in `_build/prod/rel/bakeware`.

Running it as just as described in the following sections (just make sure that you are in a the root directory of an Elixir project):

```sh
$ _build/prod/rel/bakeware/mix_version
0.1.0

$ _build/prod/rel/bakeware/mix_version up -p
Updated `mix.exs` version: "0.1.1"
```

## Usage

### Get the current version

```sh
$ _build/prod/rel/bakeware/mix_version
0.0.1
```

### Get the next patch version

```sh
$ _build/prod/rel/bakeware/mix_version get -p
0.0.2
```

### Get the next major version

```sh
$ _build/prod/rel/bakeware/mix_version get -m
1.0.0
```

### Get the next minor version

```sh
$ _build/prod/rel/bakeware/mix_version get -n
0.1.0
```

You can also combine options, and it is worth nothing that each successively higher part will zero the lower parts,

```sh
$ _build/prod/rel/bakeware/mix_version get -m -n
1.1.0

$ _build/prod/rel/bakeware/mix_version get -m -n -p
1.1.1

$ _build/prod/rel/bakeware/mix_version get -n -p
0.1.1
```

Running `_build/prod/rel/bakeware/mix_version update` will update the project's `mix.exs` file using the same methodology as above,

```sh
$ _build/prod/rel/bakeware/mix_version up -p
Updated `mix.exs` version: "0.0.5"

$ _build/prod/rel/bakeware/mix_version up -n -p
Updated `mix.exs` version: "0.1.1"
```

### Generating Git Commits

Using the `--commit`, or `-c` flag will update the `mix.exs` file and created a commit,

```sh
$ _build/prod/rel/bakeware/mix_version up -n -p -c
`mix_version` created a new git commit:
[feature-branch ab045f75a62d5e8b8c7c8de36cfd091939f5f10c] Updated mix.exs version: 0.1.1
 1 file changed, 1 insertions(+)
```
