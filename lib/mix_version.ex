defmodule MixVersion do
  @moduledoc """
  A tool for working with mix.exs project version numbers.

  Options:
  `-m`, or `--major` - updates the major number
  `-n`, or `--minor` - updates the minor number
  `-p`, or `--patch` - updates the patch number
  `--pre` - updates the pre-release version number
  `--build` - updates the build version number
  `-c`, or `--commit` - generates a commit if run with the `up` command
  """
  @opts [
    aliases: [
      m: :major,
      n: :minor,
      p: :patch,
      c: :commit,
    ],
    strict: [
      major: :boolean,
      minor: :boolean,
      patch: :boolean,
      pre:   :string,
      build: :string,
      commit: :boolean
    ]
  ]
  @sort_order Enum.map(@opts[:strict], fn {k, _} -> k end)

  use Bakeware.Script

  @impl Bakeware.Script
  def main(argv) do
    case OptionParser.parse(argv, @opts) do
      {_, _, invalid_opts} when invalid_opts != [] ->
        invalid_opts
        |> Enum.map(fn {n, _} -> "mix version: invalid option '#{n}'" end)
        |> List.insert_at(-1, "Try 'mix version --help' for more information.")
        |> Enum.join("\n")
        |> IO.puts()

      {parsed, ["up"], _} ->
        parsed
        |> increase_version!()
        |> to_string()
        |> overwrite_mix_exs!()
        |> maybe_commit(parsed[:commit])

      {parsed, ["get"], _} ->
        parsed
        |> increase_version!()
        |> to_string()
        |> IO.puts

      _ ->
        version()
        |> IO.puts()
    end
  end

  defp sort_opts(opts) do
    find_index = fn x -> Enum.find_index(@sort_order, fn _ -> x end) end
    Enum.sort(opts, fn {k1, _}, {k2, _} -> find_index.(k1) < find_index.(k2) end)
  end

  defp version do
    with regex <- ~r{version\:\s+\"([0-9\.]+)\"},
         mix_data <- File.read!("mix.exs"),
         results <- Regex.run(regex, mix_data)
    do
      List.last(results)
    end
  end

  defp version(:parsed) do
    version()
    |> Version.parse!()
  end

  defp increase_version!(opts) do
    opts
    |> sort_opts()
    |> Enum.reduce(version(:parsed), fn
      {:commit, _}, v ->
        v
      {:major, true}, v ->
        Map.merge(v, %{major: Map.get(v, :major) + 1, minor: 0, patch: 0})
      {:minor, true}, v ->
        Map.merge(v, %{minor: Map.get(v, :minor) + 1, patch: 0})
      {part, true}, v ->
        Map.put(v, part, Map.get(v, part) + 1)
      {:pre, value}, v ->
        Map.put(v, :pre, String.split(value, "."))
      {part, value}, v ->
        Map.put(v, part, value)
    end)
  end

  defp overwrite_mix_exs!(version) do
    "mix.exs"
    |> File.read!()
    |> String.replace("version: \"#{version()}\",", "version: \"#{version}\",")
    |> write!("mix.exs")

    # pass the version along!
    version
  end

  defp write!(buf, file) do
    File.write!(file, buf)
  end

  defp maybe_commit(version, true) do
    commit_msg = "Updated mix.exs version: #{version}"
    System.cmd("git", ["add", "mix.exs"])
    System.cmd("git", ["commit", "-m", commit_msg])
    {hash, 0} = System.cmd("git", ["rev-parse", "HEAD"])
    hash = String.trim_trailing(hash)

    branch = System.cmd("git", ["rev-parse", "--abbrev-ref", "HEAD"])
    |> elem(0)
    |> String.trim_trailing()

    """
    `mix_version` created a new git commit:
    [#{branch} #{hash}] #{commit_msg}
     1 file changed, 1 insertions(+)
    """
    |> IO.puts()
  end

  defp maybe_commit(version, _) do
    IO.puts("Updated `mix.exs` version: \"#{version}\"")
  end
end
